// SPDX-FileCopyrightText: 2024 James Turner <james@flightgear.org>
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @file
 * @brief Precompiled header
 */

#pragma once

#include <simgear/debug/logstream.hxx>
#include <simgear/props/props.hxx>
#include <simgear/math/SGMath.hxx>
#include <simgear/structure/SGBinding.hxx>
#include <simgear/structure/SGExpression.hxx>
#include <simgear/structure/commands.hxx>

