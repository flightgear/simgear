// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2016 James Turner <james@flightgear.org>

/**
 * @file
 * @brief  manage finding resources by names/paths
 */

#include "sg_hash.hxx"

#include <cstring>

namespace simgear
{

#include "sha1.c"

}
