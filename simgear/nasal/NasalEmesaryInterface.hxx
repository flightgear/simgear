

// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2019  Richard Harrison rjh@zaretto.com
/// @brief Nasal Emesary receipient interface.

#pragma once
namespace nasal
{

void initMainLoopRecipient();
void shutdownMainLoopRecipient();
   
} // namespace nasal
