// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2008-2012 Mathias Froehlich <mathias.froehlich@web.de>

#include <simgear_config.h>

#include "BVHPageRequest.hxx"

namespace simgear {

BVHPageRequest::~BVHPageRequest()
{
}

}
