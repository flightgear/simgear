// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2008-2009 Mathias Froehlich <mathias.froehlich@web.de>

#include "BVHStaticLeaf.hxx"
#include "BVHVisitor.hxx"

namespace simgear {

BVHStaticLeaf::~BVHStaticLeaf()
{
}

}
