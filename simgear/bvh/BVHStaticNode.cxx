// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2008-2009 Mathias Froehlich <mathias.froehlich@web.de>

#include <simgear_config.h>

#include "BVHStaticNode.hxx"

namespace simgear {

BVHStaticNode::~BVHStaticNode()
{
}

}
