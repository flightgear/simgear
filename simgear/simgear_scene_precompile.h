// SPDX-FileCopyrightText: 2024 James Turner <james@flightgear.org>
// SPDX-License-Identifier: LGPL-2.1-or-later

/**
 * @file
 * @brief Precompiled header
 */

#pragma once

#include <simgear/scene/material/EffectGeode.hxx>
#include <simgear/canvas/Canvas.hxx>
#include <simgear/bvh/BVHGroup.hxx>
#include <simgear/scene/tgdb/SGReaderWriterBTG.hxx>