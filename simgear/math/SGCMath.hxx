// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2006 Mathias Froehlich <mathias.froehlich@web.de>

#ifndef SGCMath_H
#define SGCMath_H

#include <simgear/compiler.h>

#include <cmath>

#endif
