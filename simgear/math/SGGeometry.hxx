// SPDX-License-Identifier: LGPL-2.1-or-later
// SPDX-FileCopyrightText: 2006 Mathias Froehlich <mathias.froehlich@web.de>

#ifndef SGGeometry_HXX
#define SGGeometry_HXX

// Required ...
#include "SGMath.hxx"

// Make sure all is defined
#include "SGGeometryFwd.hxx"

// Geometric primitives we know about
#include "SGBox.hxx"
#include "SGSphere.hxx"
#include "SGRay.hxx"
#include "SGLineSegment.hxx"
#include "SGPlane.hxx"
#include "SGTriangle.hxx"

// Intersection tests
#include "SGIntersect.hxx"

#endif
