// SPDX-License-Identifier: LGPL-2.1-or-later

#ifndef SIMGEAR_IO_TEST_DNS_HXX
#define SIMGEAR_IO_TEST_DNS_HXX

#include <sstream>

#include <simgear/io/sg_netChat.hxx>
#include <simgear/misc/strutils.hxx>

namespace simgear
{
} // of namespace simgear

#endif // of SIMGEAR_IO_TEST_DNS_HXX
