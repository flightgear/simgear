// SPDX-FileCopyrightText: 2002 Richard Harrison <richard@zaretto.com>

/**
 * @file
 * @brief Emesary main implementation - class based inter-object communication.
 *
 * This only needs to instance the GlobalTransmitter as all of the logic is in the header files (by design)
 *
 * @see http://www.chateau-logic.com/content/class-based-inter-object-communication
 */

#include "simgear/emesary/Emesary.hxx"

namespace simgear
{
    namespace Emesary
    {
      
    }
}
